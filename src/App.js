import { Box, Button } from '@chakra-ui/react';
import React, {useState, useMemo} from 'react'
import ScreenOne from './Containers/ScreenOne/ScreenOne';
import Layout from './Layout/MainLayout/MainLayout'

import './App.css';

function App() {
	return ( 
		<Layout>
			<ScreenOne/>
		</Layout>
	);
}

export default App;