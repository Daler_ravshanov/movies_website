import { Box, Button, HStack, Link, Text, VStack } from "@chakra-ui/react";
import ReactStars from "react-rating-stars-component";
import godzilla from "../../assets/images/godzilla.jpeg";
import React from "react";
import Carousel from "./CarouselComp";

const ratingChanged = (newRating) => {
	console.log(newRating);
};

const ScreenOne = () => {
	return (
		<Box
			w="100%"
			h="100vh"
            maxH="1080px"
			bgImg={godzilla}
			bgSize="cover"
			bgRepeat="no-repeat" 
            d="flex"
            flexDir="column"
            alignItems="flex-start"
            justifyContent="flex-end"

		>
			<VStack w="500px" alignItems="flex-start" gap={6} p="40px" >
                <Box>
                    <Text fontSize="90px" fontWeight="700" color="white" lineHeight="100px" >
                        GODZILLA
                    </Text>
                    <Text fontSize="50px" fontWeight="300" color="white" whiteSpace="nowrap" >
                        KING OF THE MONSTERS
                    </Text>
                    <Text fontSize="18px" fontWeight="300" color="#FFFFFF">
                        It is a long established fact that a reader will be
                        distracted by the readable content of a page when looking at
                        its layout. The point of using Lorem Ipsum is that it has a
                        more-or-less normal distribution of letters.
                    </Text>
                </Box>
				<ReactStars
					count={5}
					onChange={ratingChanged}
					size={24}
					activeColor="#ffd700"
				/>
				<HStack>
					<Button
						// leftIcon={<EmailIcon />}
						colorScheme="teal"
						variant="solid"
						borderRadius="30px"
					>
						Watch Now
					</Button>
					<Link color="teal"> Trailer </Link>
				</HStack>
			</VStack>
            <Box p="40px" >
			    <Carousel />
            </Box>
		</Box>
	);
};

export default ScreenOne;
