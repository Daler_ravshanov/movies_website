import { Box, HStack, Image, Link } from '@chakra-ui/react';
import React from 'react';

const MainLayout = ({children}) => {
    return (
        <Box>
            <HStack pos="absolute" top="0px" left="0px" w="100%" justifyContent="space-between" p="40px" color="white" >
                <HStack>
                    <Link to="#" >Movies</Link>
                    <Link to="#" >TV Shows</Link>
                    <Link to="#" >Documentaries</Link>
                </HStack>
                <Image src="" alt="logo" />
                <HStack>
                    <Link to="#" >
                        {/* <Icon/> */}
                    </Link>
                    <Link to="#">
                        {/* <Icon/> */}
                    </Link>
                    <Link>Sign Up</Link>
                </HStack>
            </HStack>
            {children}
        </Box>
    );
};


export default MainLayout;